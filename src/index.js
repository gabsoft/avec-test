import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
window.addEventListener(`click`, (e) => {
    if(e.target.tagName.toLowerCase() === `td`){
        window.location.href  = e.target.parentNode.getAttribute(`data-js`)
    }
})

ReactDOM.render(<App />, document.getElementById('root'));
serviceWorker.unregister();
