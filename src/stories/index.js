import React from 'react';

import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';
import { linkTo } from '@storybook/addon-links';
import { Button, Welcome } from '@storybook/react/demo';
import News from '../components/news'
import NewsList from '../components/newsList'
import Header from '../components/header'

import '../App.css'

storiesOf('Welcome', module).add('to Storybook', () => <Welcome showApp={linkTo('Button')} />);

storiesOf('Button', module)
  .add('with text', () => <Button onClick={action('clicked')}>Hello Button</Button>)
  .add('with some emoji', () => <Button onClick={action('clicked')}>😀 😎 👍 💯</Button>);


let info = {sectionName: 'Football', webTitle: 'West Ham United v Liverpool: Premier League – live!', webPublicationDate: '2019-02-04T19:11:04Z', webUrl: 'TesteUrl'}

storiesOf(`Header`, module)
  .add(`Header`, () => <Header></Header>)
  
storiesOf(`News`, module)
  .add(`News`, () => <News onClick={action(`clicked`)}{...info}/>)

storiesOf(`NewsList`, module)
  .add(`News List`, () => <NewsList onClick={action()}/>)

