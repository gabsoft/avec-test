import React from 'react'

class News extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            sectionName: props.sectionName,
            webTitle: props.webTitle,
            webPublicationDate: props.webPublicationDate,
            webUrl: props.webUrl,
            class: 'not-clicked'
        }
        this.formatDate = this.formatDate.bind(this)
        this.handleClick = this.handleClick.bind(this)
    }
    handleClick(e) {
        window.location.href  = e.target.parentNode.getAttribute(`data-js`)
    }

    formatDate() {
        let date = this.state.webPublicationDate
        let now = new Date(date)
        let day = now.getDate().toString().length === 1 ? `0${now.getDate()}` : now.getDate()
        let month = now.getMonth().toString().length === 1 ? `0${now.getMonth()}` : now.getMonth()
        let minutes = now.getMinutes().toString().length === 1 ? `0${now.getMinutes()}` : now.getMinutes()
        let hour = now.getHours().toString().length === 1 ? `0${now.getHours()}` : now.getHours()
        let seconds = now.getSeconds().toString().length === 1 ? `0${now.getSeconds()}` : now.getSeconds()
        return (
            `${day}/${month}/${now.getFullYear()} ${hour}:${minutes}:${seconds}`
        )
    }

    render() {
        return ( <tr onClick={this.handleClick} className={this.state.class} data-js={this.state.webUrl}>
            <td>{ this.state.sectionName }</td>
            <td>{ this.state.webTitle }</td>
            <td>{ this.formatDate() }</td>
        </tr>
        )
    }    
}

export default News