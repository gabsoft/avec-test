import React from 'react'
import fetch from 'isomorphic-fetch'
import News from './news'

class NewsList extends React.Component {
    state = {
        news: [],
        page: 1,
        pages: null,
        scrolling: false,
    }
    
    componentDidMount() {
        this.hydrateStateWithLocalStorage();
        window.addEventListener(
          "beforeunload",
          this.saveStateToLocalStorage.bind(this)
        );
      }
    
    componentWillMount() {
        localStorage.clear()
        this.loadNews()
        this.scrollListener = window.addEventListener('scroll', (e) => {
            this.handleScroll(e)
        })
    }

    componentWillUnmount() {
        window.removeEventListener(
            "beforeunload",
            this.saveStateToLocalStorage.bind(this)
          );
        this.saveStateToLocalStorage();
    }

    handleScroll = (e) => {
        const { scrolling, pages, page } = this.state
        if (scrolling) return
        if (pages <= page) return
        const lastTr = document.querySelector('tbody.news > tr:last-child ')
        const lastTrOffSet = lastTr.offsetTop + lastTr.clientHeight
        const pageOffSet = window.pageYOffset + window.innerHeight
        const bottomOfset = 20
        if(pageOffSet > lastTrOffSet - bottomOfset) this.loadMore()
    }

    loadNews = () => {
        const { page, news } = this.state
        const url = `https://content.guardianapis.com/search?api-key=0d160d0f-71cd-48b0-801f-2fc9cabd2157&&page=${page}`
        fetch(url)
            .then(response => response.json())
            .then(json => this.setState({
                news: [...news, ...json.response.results],
                scrolling: false,
                pages: json.response.pages
            }))
    }

    loadMore = () => {
        this.setState(prevState => ({
            page: prevState.page + 1,
            scrolling: true,            
        }), this.loadNews)
    }

    hydrateStateWithLocalStorage() {
        for (let key in this.state) {
          if (localStorage.hasOwnProperty(key)) {
            let value = localStorage.getItem(key);
            try {
              value = JSON.parse(value);
              this.setState({ [key]: value });
            } catch (e) {
              this.setState({ [key]: value });
            }
          }
        }
      }
    
      saveStateToLocalStorage() {
        for (let key in this.state) {
          localStorage.setItem(key, JSON.stringify(this.state[key]));
        }
      }
    render() {
        return <table>
            <thead>
                <tr>
                    <th>Section</th>
                    <th>News Title</th>
                    <th>Publication Date</th>
                </tr>
            </thead>

            <tbody className="news">
                {
                    this.state.news.map((map, i) => 
                            <News {...map} key={i} />
                    )
                }
            </tbody> 
            </table>

    }
}

export default NewsList