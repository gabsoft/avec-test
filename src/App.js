import React, { Component } from 'react'
import NewsList from './components/newsList'
import Header from './components/header'
import './App.css';
import { BrowserRouter, Route } from 'react-router-dom'

class App extends Component {
  render() {
    return (
      <div>
        <Header/>
        <BrowserRouter>
          <Route path='/' component={NewsList} exact/>
        </BrowserRouter>
      </div>
    )
  }
}

export default App;
